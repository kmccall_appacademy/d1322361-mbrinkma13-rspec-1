# Mike Brinkman
# 6/25/17

def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, repeat = 2)
  result_arr = []
  i = 0
  while i < repeat
    result_arr << str
    i += 1
  end

  result_arr.join(' ')
end

def start_of_word(word, num)
  word[0..(num - 1)]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  lil_words = ['the', 'and', 'or', 'to', 'over']
  new_str_arr = []

  str.split.each_with_index do |w, i|
    if lil_words.include?(w) == false || i == 0
      new_str_arr << w.capitalize
    else
      new_str_arr << w
    end
  end

  new_str_arr.join(' ')
end
