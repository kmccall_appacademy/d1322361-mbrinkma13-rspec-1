# Mike Brinkman
# 6/25/17

def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  arr.empty? ? 0 : arr.reduce(:+)
end

def multiply(*nums)
  nums.to_a.reduce(:*)
end

def power(num, pow)
  num ** pow
end

def factorial(num)
  num == 0 ? 0 : (1..num).reduce(:*)
end
