# Mike Brinkman
# 6/26/17

def translate(words)
  word_arr = words.split(' ')
  pig_words = []

  word_arr.each { |w| pig_words << pigify(w) }

  pig_words.join(' ')
end

def pigify(word)
  vowels = 'aeiou'

  if vowels.include?(word[0])
    word + 'ay'
  else
    pig_consonant(word)
  end
end

def pig_consonant(word)
  vowels = 'aeiou'
  word_arr = word.split('')

  while (vowels.include?(word_arr[0]) == false) || (word_arr[0] == 'u' && word_arr.last == 'q')
    word_arr.push(word_arr[0])
    word_arr.delete_at(0)
  end

  word_arr.join('') + 'ay'
end
